---
title: 服务器时间与数据库时间未同步产生的bug
id: linux-db-no-sync-time-bug
tags: ["运维","bug"]
date: 2018-07-09T11:42:08
category: bug笔记
priority: 0
---

最近接了个做投票网站的活，每人每天可以投票3次。查数据时，突然发现有人在零点的时候投了几百张票！有人在搞事情呀！查了半天才发现服务器和数据库时间不一致，然后检查每人每天限制时，又分别查了两个地方！
<!--more-->

## 投票设计

### 1. 数据库
在`vote`表里，每投一次票，就插入一条数据
```mysql
#只是个伪代码，示意一下
insert into vote(id,user_id,team_id,create_time)
```
### 2. 每天一个人限制三次
插入前，先统计一下，这个人投票次数
```
#就是查询下，当前时间的天，该用户投票数据的统计
select count(0) from vote v where v.user_id = :userId and to_days(v.create_t) = to_days(now())
```
## 被刷票了！
查下数据，发现有个人短短几天内投了几百张票！看数据库记录还都是零点前的2分钟内。11:58-00:00 这个时间段。
奇怪了，反复检查代码，认为不可能出错。然后...在服务器上执行下
```
date
Mon Jul  9 15:06:35 CST 2018
```
好像比我的电脑慢了2分钟，再在数据库执行下
```
select NOW();
```
一对比，服务器比数据库慢了2分钟！

然后就明白了，数据库里的`create_time`是用代码`new Date()`的，是服务器时间，所以，记录是11:58分，实际上，已经过了0点了。那么这个时间段内执行的
```mysql
to_days(v.create_t) = to_days(now())
#因为 create_t 一直都是0点以前的，而now()是0点以后。所以统计出的票数一直是0，所以他能一直投票！
```
## 解决
1. 代码层面上，插入时间使用数据库时间！
2. 把服务器和数据库时间同步一下。

最后我只是把服务器时间和网络时间同步了一下。
执行的命令：

```
ntpdate  0.cn.pool.ntp.org
```
参考：https://www.chenyudong.com/archives/linux-ntpdate-time-synchronize.html



#### 以后一定要注意服务器时间和数据库时间的问题，其实不止这些！有些东西的判断标准必须统一！

